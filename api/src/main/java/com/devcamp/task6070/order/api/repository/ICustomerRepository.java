package com.devcamp.task6070.order.api.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070.order.api.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer,Long> {

    CCustomer findById(int customerId);

    
}
