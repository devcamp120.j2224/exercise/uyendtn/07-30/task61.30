package com.devcamp.task6070.order.api.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;



@Entity
@Table(name = "customer")
public class CCustomer {

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private int   id;

@Column(name = "ho_ten")
private String	fullname;

@Column(name = "email")
private String	email;

@Column(name = "dien_thoai")
private String	phone;

@Column(name = "dia_chi")
private String	Address;

@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
@JsonManagedReference
private List<COrder> orders;


public CCustomer() {
}
public CCustomer(int id, String fullname, String email, String phone, String address, List<COrder> orders) {
    this.id = id;
    this.fullname = fullname;
    this.email = email;
    this.phone = phone;
    Address = address;
    this.orders = orders;
}
public long getId() {
    return id;
}
public void setId(int id) {
    this.id = id;
}
public String getFullname() {
    return fullname;
}
public void setFullname(String fullname) {
    this.fullname = fullname;
}
public String getEmail() {
    return email;
}
public void setEmail(String email) {
    this.email = email;
}
public String getPhone() {
    return phone;
}
public void setPhone(String phone) {
    this.phone = phone;
}
public String getAddress() {
    return Address;
}
public void setAddress(String address) {
    Address = address;
}
public List<COrder> getOrders() {
    return orders;
}
public void setOrders(List<COrder> orders) {
    this.orders = orders;
}



}
